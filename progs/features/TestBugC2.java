class Main {
    public static void main(String[] args){
		System.out.println(new TestBugC2().Start());
	}
}

        // this program checks to see if the bug return false when x equals y.
        
class TestBugC2 {
	public int Start() {
		int x;
		int y;
		
		x = 5;
		y = 5;
	
		if (x <= y) {
			System.out.println(1);
		} else {
			System.out.println(0);
		}
		
	// if the compilier works it will return 1 .
		
		return 0;
	}
}

