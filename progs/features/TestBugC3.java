class Main {
    public static void main(String[] args){
		System.out.println(new TestBugC3().Start());
	}
}

            // This program checks to see if compiler, <= has lower precedence than &&.
           // If compiler works,it will return  1,
         
class TestBugC3 {
	public int Start() {
		int x;
		int y;
		int z;
		
		x = 5;
		y = 5;
		z = 4;
	
		if (z <= x && x <= y) {
			System.out.println(1);
		} else {
			System.out.println(0);
		}
		
		return 0;
	}
}

