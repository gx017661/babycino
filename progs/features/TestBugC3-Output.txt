ubuntu@ubuntu:~/babycino$ ./build.sh
ubuntu@ubuntu:~/babycino$ ./babycino.sh progs/features/TestBugC3.java progs/features/TestBugC3.c
Expected boolean as 1st argument to &&; actual type: int
Context: x&&x
Expected boolean as 2nd argument to &&; actual type: int
Context: x&&x
Expected int as 2nd argument to <=; actual type: boolean
Context: z<=x&&x
Expected int as 1st argument to <=; actual type: boolean
Context: z<=x&&x<=y
Exiting due to earlier error.
